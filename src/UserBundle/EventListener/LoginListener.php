<?php
namespace UserBundle\EventListener;

use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class LoginListener
{
    protected $twig;
    protected $container;

    public function __construct(\Twig_Environment $twig, \Symfony\Component\DependencyInjection\Container $container) {
        $this->twig = $twig;
        $this->container = $container;
    }

    public function login(FilterControllerEvent $event)
    {   
        $csrfToken = $this->container->get('security.csrf.token_manager')->getToken('authenticate')->getValue();
        $this->twig->addGlobal('login_csrfToken', $csrfToken);
    }
}



    