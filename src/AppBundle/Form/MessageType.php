<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;


class MessageType extends AbstractType{

    /**
    * @param FormBuilderInterface $builder
    * @param array $options
    */


    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
                ->add('mail', TextType::class)             
                ->add('firstName', TextType::class)
                ->add('lastName', TextType::class)
                ->add('title', TextType::class)
                ->add('content', TextareaType::class)
                ->add('send', SubmitType::class, ['label' => "Envoyer"])
                ->add('recaptcha', EWZRecaptchaType::class);
    }

    public function configureOptions(OptionsResolver $resolver){
        $resolver -> setDefaults([
            'data_class' => "AppBundle\Entity\Message",
            ]);

    }
}

  