<?php  

namespace AppBundle\Form\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\IsTrue as RecaptchaTrue;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;


class CreationType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options){
		
		$builder
			->add('category', EntityType::class, array(
					    'class' => 'AppBundle:Category',
					    'query_builder' => function (EntityRepository $er) {
					        return $er->createQueryBuilder('u')
					        	->where('u.display = 1')
					            ->orderBy('u.name', 'ASC');
					    },
					    'choice_label' => 'name',
					))
			->add('name', TextType::class)
			->add('content', TextareaType::class)
			->add('media', EntityType::class, array(
					    'class' => 'AppBundle:Media',
					    'query_builder' => function (EntityRepository $er) {
					        return $er->createQueryBuilder('u')
					            ->orderBy('u.id', 'ASC');
					    },
					    'choice_label' => 'name',
					))			
			->add('valid', SubmitType::class, ['label' => 'Valider']);
	}

	public function configureOptions(OptionsResolver $resolver){

		$resolver->setDefaults([
			'data_class' => 'AppBundle\Entity\Creation',
		]);
	}
	


}

?>