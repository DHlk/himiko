<?php
namespace AppBundle\Entity;


use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 * @ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable = true)
     * @Expose
     * @Assert\NotBlank()
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable = true)
     * @Expose
     * @Assert\NotBlank()
     */
    private $lastName;

    /**
     * @var display
     * @ORM\Column(name="display", type="boolean", options={"default":true})
     */
    private $display=1;


    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * One User has Many Comments
     * @ORM\OneToMany(targetEntity="CreationComment", mappedBy="user")
     */
    private $creationComments;

    /**
     * Set display
     *
     * @param boolean $display
     *
     * @return Category
     */
    public function setDisplay($display)
    {
        $this->display = $display;

        return $this;
    }

     /**
     * Get display
     *
     * @return boolean
     */
    public function getDisplay()
    {
        return $this->display;
    }
    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
        return $this;
    }




    /**
     * Add creationComment
     *
     * @param \AppBundle\Entity\CreationComment $creationComment
     *
     * @return User
     */
    public function addCreationComment(\AppBundle\Entity\CreationComment $creationComment)
    {
        $this->creationComments[] = $creationComment;

        return $this;
    }

    /**
     * Remove creationComment
     *
     * @param \AppBundle\Entity\CreationComment $creationComment
     */
    public function removeCreationComment(\AppBundle\Entity\CreationComment $creationComment)
    {
        $this->creationComments->removeElement($creationComment);
    }

    /**
     * Get creationComments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCreationComments()
    {
        return $this->creationComments;
    }
}
