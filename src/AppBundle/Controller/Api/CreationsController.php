<?php

namespace AppBundle\Controller\Api;

use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;


class CreationsController extends FOSRestController
{
	/**
	 * @Rest\Get("/creations")
	 * @ApiDoc(
     *  resource=true,
     *  description="Toutes les créations",
     *  )
	 */
	public function getAction(Request $request) {
		$em=$this->getDoctrine()->getManager();
		$deals=$em->getRepository("AppBundle:Creation")->findAll();
		return $creations;
	}

	/**
	 * @Rest\Get("/creations/{id}")
	 * @ApiDoc(
     *  resource=true,
     *  description="Une seule création",
     *  )
	 */
	public function getCreation(Request $request, $id) {
		$em=$this->getDoctrine()->getManager();
		$deal=$em->getRepository("AppBundle:Creation")->find($id);

		return $creation;
	}

	/**
	 * @Rest\Post("/creations/search")
	 * @ApiDoc(
     *  resource=true,
     *  description="Recherche de création",
     *  )
	 */
	public function searchCreation(Request $request) {
		$em=$this->getDoctrine()->getManager();
		$keywords = $request->query->get("keywords");
		$dql = "SELECT a FROM AppBundle:Creation where a.name like :name";
		$query = $em->createQuery($dql);
		$query->setParameters(["name"=> "%". $keywords . "%",
			]);
		$creations = $query->getResult();
		return $creations;
	}



}

