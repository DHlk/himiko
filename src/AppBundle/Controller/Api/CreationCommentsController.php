<?php

namespace AppBundle\Controller\Api;

use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Creation;
use AppBundle\Entity\CreationComment;
use AppBundle\Form\Api\CreationCommentType;
use AppBundle\Entity\User;


class CreationCommentsController extends FOSRestController
{

    /**
     * @Rest\Get("/comments")
     */
    public function getAllAction(Request $request) {

    	$em = $this->getDoctrine()->getManager();

    	$creationComments = $em->getRepository('AppBundle:CreationComment')->findAll();

    	return $creationComments;
    }


    /**
     * @Rest\Get("/creation/{id}", name="viewcreation_api")
     */
    public function getAction(Request $request, $id) {

    	$em = $this->getDoctrine()->getManager();

    	$creationComment = $em->getRepository('AppBundle:CreationComment')->findOneById($id);
      $creation = $em->getRepository('AppBundle:Creation')->findOneById($id);


      return $this->render('AppBundle:Web/Creations:creation.html.twig', [
          "creationComment" => $creationComment,
          "creation" => $creation,
      ]);
    }


    /**
     * @Rest\Delete("/comments/{id}")
     */
    public function deleteCreationComment(Request $request, $id){

    	$em = $this->getDoctrine()->getManager();

    	$creationComment = $em->getRepository('AppBundle:CreationComment')->findOneById($id);

    	$creationComment->setDisplay(false);

    	$em->persist($creationComment);
    	$em->flush();

    	return $creationComment;

    }


    /**
     * @Rest\Put("/comments/{id}")
     */
    public function editCreationComment(Request $request, $id){

        $em = $this->getDoctrine()->getManager();

        $creationComment = $em->getRepository('AppBundle:CreationComment')->find($id);

        $form = $this->createForm(CreationCommentType::class, $comment, [
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ]);
        $form->remove('recaptcha');
        $form->submit($request->request->all());

        if($form->isValid()){

            $em->persist($creationComment);
            $em->flush();
            return $creationComment;

        } else {
            return $form;
        }
    }


    /**
     * @Rest\Post("/comments")
     */
    public function addComment(Request $request){

        $em = $this->getDoctrine()->getManager();

        $creationComment = new CreationComment();

        $form = $this->createForm(CreationCommentType::class, $creationComment, [
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ]);
        $form->remove('recaptcha');
        $form->submit($request->request->all());

        if($form->isValid()){

            $user = $em->getRepository('AppBundle:User')->find($request->request->get("user"));
            $deal = $em->getRepository('AppBundle:Creation')->find($request->request->get("creation"));

            $comment->setUser($user);
            $comment->setCreation($creation);

            $em->persist($creationComment);
            $em->flush();
            return $creationComment;

        } else {

            return $form;

        }

    }


}
