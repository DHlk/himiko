<?php

namespace AppBundle\Controller\Api;

use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Deal;
use AppBundle\Entity\Message;


class MessagesController extends FOSRestController
{
    
    /**
     * @Rest\Get("/messages")
     */
    public function getAction(Request $request) {

    	$em = $this->getDoctrine()->getManager();

    	$messages = $em->getRepository('AppBundle:Message')->findAll();

    	return $messages;
    }

}



