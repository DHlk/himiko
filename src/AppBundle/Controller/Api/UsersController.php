<?php

namespace AppBundle\Controller\Api;


use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use AppBundle\Entity\User;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;



class UsersController extends FOSRestController {

	/**
	 * @Rest\Post("/users/login")
	 * @ApiDoc(
     *  resource=true,
     *  description="Tous les utilisateurs",
     *  )
	 */
	public function loginAction(Request $request)
	{
	    $manager = $this->get('fos_user.user_manager');
	    $factory = $this->get('security.encoder_factory');
	    
	    $email = $request->request->get('email');
	    $password = $request->request->get('password');

	    $em = $this->getDoctrine()->getManager();

	    $bool = false;

	    $response = [
	        'status' => false
	    ];

	    $user = $manager->findUserByEmail($email);

	    if(is_null($user)) {
	    	throw $this->createNotFoundException('Benjamin is the culprit !');
	    } else {		    	
	    	$encoder = $factory->getEncoder($user);
		    	    
		    $bool = ($encoder->isPasswordValid($user->getPassword(), $password, $user->getSalt())) ? true : false;

		    if($bool) {
		        $response = [
		            'status' => true,
		            'session' => [
		                'id' => $user->getId(),
		                'firstName' => $user->getFirstName(),
		                'lastName' => $user->getLastName(),
		            ],
		        ];
		    }


	    return $response;
		}
	}



	/**
	 * @Rest\Post("/users/register")
	 * @ApiDoc(
     *  resource=true,
     *  description="Tous les utilisateurs enregistrés",
     *  )
	 */
	public function registerAction(Request $request) {

        $formFactory = $this->get('fos_user.registration.form.factory');
        $userManager = $this->get('fos_user.user_manager');
        $dispatcher = $this->get('event_dispatcher');
        $loginError = $this->get('security.authentication_utils')->getLastAuthenticationError();

        $user = $userManager->createUser();
        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm([
        	'csrf_protection' => false,
        	'allow_extra_fields' => true,
        ]);
        $form->remove('recaptcha');
        $form->setData($user);
        $form->submit($request->request->all());
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user->setEnabled(true);
            $em->persist($user);
            $em->flush();

            return $user;
        } else {
        	return $form;
        }
    }

}