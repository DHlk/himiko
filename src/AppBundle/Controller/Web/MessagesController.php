<?php

namespace AppBundle\Controller\Web;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Message;
use AppBundle\Form\MessageType;
use AppBundle\Entity\User;

class MessagesController extends Controller {
    
    /**
     *  @Route("/contact", name="contact")
     */

    public function contact(Request $request){
       
        $message = new Message();

        $user = $this->getUser();
        if($user != null) {
            $message->setFirstName($user->getFirstName());
            $message->setLastName($user->getLastName());
            $message->setMail($user->getEmail());
            $message->setUser($user);
        }

        $form = $this->createForm(MessageType::class, $message);

        $form->handleRequest($request);
        
        if($form->isSubmitted() &&  $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($message);
            $em->flush();
     
            
            return $this->redirectToRoute("homepage");
        }

        return $this->render('AppBundle:Web/Messages:contact.html.twig', [
            "form" => $form->createView(),
        ]);

    }

}