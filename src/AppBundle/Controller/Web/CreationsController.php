<?php

namespace AppBundle\Controller\Web;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Deal;
use AppBundle\Entity\CreationComment;
use AppBundle\Form\CreationCommentType;




class CreationsController extends Controller
{
    /**
     * @Route("/creations/{id}", name="viewcreations")
     */
    public function indexAction(Request $request, $id)
    {
    	$em = $this->getDoctrine()->getManager();
    	$dql = "SELECT a FROM AppBundle:Creation a JOIN a.category c WITH c.id = :id";
      $query = $em->createQuery($dql);
      $query->setParameter('id', $id);

      $paginator  = $this->get('knp_paginator');
      $pagination = $paginator->paginate(
          $query, /* query NOT result */
          $request->query->getInt('page', 1)/*page number*/,
          10/*limit per page*/
      );


        return $this->render('AppBundle:Web/Creations:creations.html.twig', [
        	"creations" => $pagination,

        	]);
    }


    /**
     * @Route("/creation/{id}", name="viewcreation")
     */
    public function viewCreation(Request $request, $id)
    {
      $em = $this->getDoctrine()->getManager();

      $creation = $em->getRepository('AppBundle:Creation')->findOneById($id);
      $creationComments = $creation->getComments();


      $creationComment = new CreationComment();
      $form = $this->createForm(CreationCommentType::class, $creationComment);

      $form->handleRequest($request);

      if($form->isSubmitted() &&  $form->isValid()){
          $em = $this->getDoctrine()->getManager();

          $creationComment->setUser($user = $this->getUser());
          $creationComment->setCreation($creation);
          $creationComment->setDisplay("1");

          $em->persist($creationComment);
          $em->flush();
          return $this->redirectToRoute("viewcreation", [
            "id" => $id,
          ]);
      }

        return $this->render('AppBundle:Web/Creations:creation.html.twig', [
          "creation" => $creation,
          "creationComments" => $creationComments,
          "form" => $form->createView(),

          ]);
    }

}
