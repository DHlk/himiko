<?php

namespace AppBundle\Controller\Web;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Creation;
use AppBundle\Entity\CreationComment;
use AppBundle\Form\Api\CreationCommentType;

class PagesController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
    	
    	$em = $this->getDoctrine()->getManager();
	    $keywords = $request->query->get("keywords");	
	    $dql = "SELECT a FROM AppBundle:Creation a";

	    $query = $em->createQuery($dql);

	    $paginator  = $this->get('knp_paginator');
	    $pagination = $paginator->paginate(
	        $query, /* query NOT result */
	        $request->query->getInt('page', 1)/*page number*/,
	        10/*limit per page*/
	    );

	    $categories = $em->getRepository('AppBundle:Category')->findAll();

	    // parameters to template
	    return $this->render('AppBundle:Web/Pages:homepage.html.twig', [
	    	"pagination" => $pagination,
	    	"categories" => $categories,
	    ]);

	}
    

    /**
     * @Route("/search", name="search")
     */
    public function listAction(Request $request)
	{
	    $em = $this->getDoctrine()->getManager();
	    $keywords = $request->query->get("keywords");
	    $categories = $request->query->get("categories");	
	    $dql = "SELECT a FROM AppBundle:Creation a INNER JOIN AppBundle:Category c WITH a.category = c.id WHERE a.name like :name AND c.name like :catname ";

	    $query = $em->createQuery($dql);
	    $query->setParameters([
	    	"name" => "%" . $keywords . "%",
	    	"catname" => "%" . $categories . "%",
	    ]);


	    $paginator  = $this->get('knp_paginator');
	    $pagination = $paginator->paginate(
	        $query, /* query NOT result */
	        $request->query->getInt('page', 1)/*page number*/,
	        10/*limit per page*/
	    );

	    // parameters to template
	    return $this->render('AppBundle:Web/Pages:homepage.html.twig', array('pagination' => $pagination));
	}

}