<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Creation;
use AppBundle\Form\Admin\CreationType;
use AppBundle\Form\Admin\MediaType;
use AppBundle\Entity\CreationComment;
use AppBundle\Entity\Media;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;

class MediasController extends Controller
{
/**
   * Upload media
   *
   * @Route("/medias/upload",
   *     name = "article_medias_upload",
   * )
   * @param  Request
   * @return json
   */
  public function uploadAction(Request $request)
  {
      $em = $this->getDoctrine()->getManager();

      $media = new Media();
      $mediasFolder = $media->getUploadRootDir();
      $i = 0;


      foreach($request->files as $uploadedFile) {
          $response = $this->checkMimeType($uploadedFile);

          if($response == false) {
              return new JsonResponse([
                  "fileError" => true,
              ]);
          }

          $name = $uploadedFile->getClientOriginalName();

          try {
              $extension = $uploadedFile->guessExtension();
          } catch (Exception $e) {
              $extension = "png";
          }

          $file_name = $name;

          $file = $uploadedFile->move($mediasFolder, $file_name);

          $media->setName($file_name);
          $em->persist($media);
          $em->flush();

          $i++;
      }


      if($i > 0) {
        $url = $this->container->get('router')->getContext()->getBaseUrl();

        return new JsonResponse(
          [
                  "id" => $media->getId(),
               "file_src" => $url . '/medias/' . $name.".".$extension,
              "file_name" => $name . "." . $extension,
          ]
        );
      }

      exit;
  }

      private function checkMimeType($uploadedFile)  {
        $mime = $uploadedFile->getMimeType();

        switch ($mime) {
            case 'image/jpeg':
                return true;
                break;

            case 'image/png':
                return true;
                break;

            case 'image/jpg':
                return true;
                break;
        }

        return false;
    }

    /**
     *
     * @Method({"GET", "POST"})
     * @Route("/ajax/snippet/image/send", name="ajax_snippet_image_send")
     */
    public function ajaxSnippetImageSendAction(Request $request)
    {
        $em = $this->container->get("doctrine.orm.default_entity_manager");

        $media = new Media();
        $media = $request->files->get('file');

        $media->setFile($media);//media
        $media->setPath($media->getPathName());
        $media->setName($media->getClientOriginalName());
        $media->upload();
        $em->persist($media);
        $em->flush();

        //infos sur le document envoyé

        return new JsonResponse(array('success' => true));
    }

    /**
     * @Route("/medias", name="admin_viewmedias")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $dql = "SELECT a FROM AppBundle:Media a ORDER BY a.name ASC";

        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );


        // parameters to template
        return $this->render('AppBundle:Admin/Medias:medias.html.twig', [
            "pagination" => $pagination,
        ]);
    }

    /**
     * @Route("/medias/display/{id}", name="admin_displaymedia", options={"expose"=true})
     */
    public function displayMedia(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $media = $em->getReference('AppBundle:Media', $id);

        if ($media->getDisplay()) {
            $media->setDisplay(false);
        } else {
            $media->setDisplay(true);
        }

        $em->persist($media);
        $em->flush();

        // parameters to template
        return $this->redirectToRoute("admin_viewmedias");
    }

    /**
     * @Route("/medias/delete/{id}", name="admin_deletemedia", options={"expose"=true})
     */
    public function deleteMedia(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $repository = $this->getDoctrine()->getRepository('AppBundle:Media');
        $media = $repository ->find($id);

        $em->remove($media);
        $em->flush();

        // parameters to template
        return $this->redirectToRoute("admin_viewmedias");
    }

    /**
     * @Route("/medias/form", name="admin_addmedia")
     */
    public function addMedia(Request $request)
    {

        $media = new Media();

        $form = $this->createForm(MediaType::class, $media);

        $form->handleRequest($request);

        if($form->isSubmitted() &&  $form->isValid()){

            $em = $this->getDoctrine()->getManager();
            //$slug = $this->get('slugify')->slugify($media->getName());
           // $media->setSlug($slug);
            $em->persist($media);
            $em->flush();
            return $this->redirectToRoute("admin_viewmedias");
        }

        return $this->render('AppBundle:Admin/Medias:formmedia.html.twig', [
            "form" => $form->createView(),
        ]);

    }

    /**
     * @Route("/medias/edit/{id}", name="admin_editmedia")
     */
    public function editMedia(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $media = $em->getRepository('AppBundle:Media')->findOneById($id);

         $form = $this->createForm(MediaType::class, $media);

        $form->handleRequest($request);

        if($form->isSubmitted() &&  $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            //$slug = $this->get('slugify')->slugify($media->getName());
            //$creation->setSlug($slug);
            $em->persist($media);
            $em->flush();
            return $this->redirectToRoute("admin_view");
        }

        return $this->render('AppBundle:Admin/Medias:formmedia.html.twig', [
            "form" => $form->createView(),
        ]);
    }

    /**
     * @Route("/media/{id}", name="admin_viewmedia")
     */
    public function viewMedia(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $media = $em->getRepository('AppBundle:Media')->findOneById($id);

        return $this->render('AppBundle:Admin/Medias:viewmedia.html.twig', [
            "media" => $media,

        ]);
    }



}
