<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Category;
use AppBundle\Form\Admin\CategoryType;


class CategoriesController extends Controller
{

	/**
     * @Route("/categories", name="admin_viewcategories")
     */
    public function indexAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
      //  $dql = "SELECT c FROM AppBundle:Category c where c.display = true";
        $dql = "SELECT c FROM AppBundle:Category c";

        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );


        // parameters to template
        return $this->render('AppBundle:Admin/Categories:categories.html.twig', [
            "pagination" => $pagination,
        ]);
    }

    /**
     * @Route("/categories/display/{id}", name="admin_displaycategory", options={"expose"=true})
     */
    public function displayCategory(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
       
        $category = $em->getReference('AppBundle:Category', $id);

        if($category->getDisplay()) {
        	$category->setDisplay(false);
        } else {
        	$category->setDisplay(true);
        }

        $em->persist($category);
        $em->flush();

        // parameters to template
        return $this->redirectToRoute("admin_viewcategories");
    }

    /**
     * @Route("/categories/delete/{id}", name="admin_deletecategory", options={"expose"=true})
     */
    public function deleteCategory(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $repository = $this->getDoctrine()->getRepository('AppBundle:Category');
        $category = $repository ->find($id);           
     
        $em->remove($category);
        $em->flush();

        // parameters to template
        return $this->redirectToRoute("admin_viewcategories");
    }

    /**
     * @Route("/categories/form", name="admin_addcategory")
     */
    public function addCategory(Request $request)
    {
        
        $category = new Category();

        $form = $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);
        
        if($form->isSubmitted() &&  $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $category->setDisplay(1);
            $em->persist($category);
            $em->flush();
            return $this->redirectToRoute("admin_viewcategories");
        }

        return $this->render('AppBundle:Admin/Categories:formcategory.html.twig', [
            "form" => $form->createView(),
        ]);

    }

    /**
     * @Route("/categories/edit/{id}", name="admin_editcategory")
     */

    public function editCategory(Request $request, $id)
    { 
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('AppBundle:Category')->findOneById($id);

        $form = $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);
       
        if($form->isSubmitted() &&  $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute("admin_viewcategories");
        }


        return $this->render('AppBundle:Admin/Categories:editcategory.html.twig', [
            "form" => $form->createView(),
        ]);


    }

    /**
     * @Route("/category/{id}", name="admin_viewcategory")
     */
    public function viewCategory(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('AppBundle:Category')->findOneById($id);

        
        return $this->render('AppBundle:Admin/Categories:viewcategory.html.twig', [
            "category" => $category,
        ]);
    }


}