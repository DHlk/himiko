<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Creation;
use AppBundle\Form\Admin\CreationType;
use AppBundle\Entity\CreationComment;
use AppBundle\Entity\Media;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;



class CreationsController extends Controller
{

    /**
     *
     * @Method({"GET", "POST"})
     * @Route("/ajax/snippet/image/send", name="ajax_snippet_image_send")
     */
    public function ajaxSnippetImageSendAction(Request $request)
    {
        $em = $this->container->get("doctrine.orm.default_entity_manager");

        $media = new Media();
        $media = $request->files->get('file');

        $media->setFile($media);
        $media->setPath($media->getPathName());
        $media->setName($media->getClientOriginalName());
        $media->upload();
        $em->persist($media);
        $em->flush();

        //infos sur le document envoyé
        //var_dump($request->files->get('file'));die;
        return new JsonResponse(array('success' => true));
    }

    /**
     * @Route("/creations", name="admin_viewcreations")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $dql = "SELECT a FROM AppBundle:Creation a";

        $query = $em->createQuery($dql);


        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );


        // parameters to template
        return $this->render('AppBundle:Admin/Creations:creations.html.twig', [
            "pagination" => $pagination,
        ]);
    }

    /**
     * @Route("/creations/display/{id}", name="admin_displaycreation", options={"expose"=true})
     */
    public function displayCreation(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $creation = $em->getReference('AppBundle:Creation', $id);

        if ($creation->getDisplay()) {
            $creation->setDisplay(false);
        } else {
            $creation->setDisplay(true);
        }

        $em->persist($creation);
        $em->flush();

        // parameters to template
        return $this->redirectToRoute("admin_viewcreations");
    }

    /**
     * @Route("/creations/delete/{id}", name="admin_deletecreation", options={"expose"=true})
     */
    public function deleteCreation(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $repository = $this->getDoctrine()->getRepository('AppBundle:Creation');
        $creation = $repository ->find($id);

        $em->remove($creation);
        $em->flush();

        // parameters to template
        return $this->redirectToRoute("admin_viewcreations");
    }


    /**
     * @Route("/creations/form", name="admin_addcreation")
     */
    public function addCreation(Request $request)
    {

        $creation = new Creation();


        $form = $this->createForm(CreationType::class, $creation);

        $form->handleRequest($request);

        if($form->isSubmitted() &&  $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            //$slug = $this->get('slugify')->slugify($creation->getName());
            //$creation->setSlug($slug);


            $em->persist($creation);
            $em->flush();
            return $this->redirectToRoute("admin_viewcreations");
        }

        return $this->render('AppBundle:Admin/Creations:formcreation.html.twig', [
            "form" => $form->createView(),
        ]);

    }

    /**
     * @Route("/creations/edit/{id}", name="admin_editcreation")
     */
    public function editCreation(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $creation = $em->getRepository('AppBundle:Creation')->findOneById($id);

         $form = $this->createForm(CreationType::class, $creation);

        $form->handleRequest($request);

        if($form->isSubmitted() &&  $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            //$slug = $this->get('slugify')->slugify($creation->getName());
            //$creation->setSlug($slug);
            $em->persist($creation);
            $em->flush();
            return $this->redirectToRoute("admin_view");
        }

        return $this->render('AppBundle:Admin/Creations:formcreation.html.twig', [
            "form" => $form->createView(),
        ]);
    }

    /**
     * @Route("/creation/{id}", name="admin_viewcreation")
     */
    public function viewCreation(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $creation = $em->getRepository('AppBundle:Creation')->findOneById($id);

        $comments = $creation->getComments();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $comments,
            $request->query->getInt('page', 1),
            10
        );

        
        return $this->render('AppBundle:Admin/Creations:viewcreation.html.twig', [
            "creation" => $creation,
            "comments" => $pagination,
        ]);
    }



}
