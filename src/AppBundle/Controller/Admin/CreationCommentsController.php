<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\User;
use AppBundle\Form\Admin\UserType;

class CreationCommentsController extends Controller
{

    /**
     * @Route("/comments", name="admin_viewcomments")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $dql = "SELECT a FROM AppBundle:CreationComment a";

        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );


        // parameters to template
        return $this->render('AppBundle:Admin/Comments:comments.html.twig', [
            "pagination" => $pagination,
        ]);
    }

    
    /**
     * @Route("/comment/delete/{id}", name="admin_deletecomment", options={"expose"=true})
     */
    public function deleteComment(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
       
        $comment = $em->getReference('AppBundle:CreationComment', $id);
        if($comment->getDisplay()) {
            $comment->setDisplay(false);
        } else {
            $comment->setDisplay(true);
        }

        $em->persist($comment);
        $em->flush();

        // parameters to template
        return $this->redirectToRoute("admin_viewcreation", ["id" => $comment->getCreation()->getId()]);
    }

    


}