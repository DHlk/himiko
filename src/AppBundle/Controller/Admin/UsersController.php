<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\User;
use AppBundle\Form\Admin\UserType;

class UsersController extends Controller
{
    /**
     * @Route("/users", name="admin_viewusers")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $dql = "SELECT u FROM AppBundle:User u";

        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );


        // parameters to template
        return $this->render('AppBundle:Admin/Users:users.html.twig', [
            "pagination" => $pagination,
        ]);
    }

    /**
     * @Route("/users/ban/{id}", name="admin_banuser", options={"expose"=true})
     */
    public function banUser(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em->getReference('AppBundle:User', $id);
        if($user->isEnabled()) {
            $user->setDisplay(false);
            $user->setEnabled(false);
        } else {
            $user->setEnabled(true);
            $user->setDisplay(true);
        }

        $em->persist($user);
        $em->flush();
        // parameters to template
        return $this->redirectToRoute("admin_viewusers");
    }

    /**
     * @Route("/users/delete/{id}", name="admin_deleteuser", options={"expose"=true})
     */
    public function deleteUser(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $repository = $this->getDoctrine()->getRepository('AppBundle:User');
        $user = $repository ->find($id);

        $em->remove($user);
        $em->flush();
        // parameters to template
        return $this->redirectToRoute("admin_viewusers");
    }


    /**
     * @Route("/user/search", name="admin_findid")
     */
    public function findUser(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $keyword = $request->query->get('keyword');

        $dql = "SELECT a FROM AppBundle:User a WHERE a.username like :username";

        $query = $em->createQuery($dql);
        $query->setParameters([
            "username" => "%" . $keyword . "%",
        ]);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render("AppBundle:Admin/Users:users.html.twig", [
            "pagination" => $pagination,
        ]);
    }

    public function registerAction(Request $request) {

      /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
      $formFactory = $this->get('fos_user.registration.form.factory');
      /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
      $userManager = $this->get('fos_user.user_manager');
      /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
      $dispatcher = $this->get('event_dispatcher');
      $cities = array();
      $loginError = $this->get('security.authentication_utils')->getLastAuthenticationError();

      $user = $userManager->createUser();
      $event = new GetResponseUserEvent($user, $request);
      $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

      if (null !== $event->getResponse()) {
          return $event->getResponse();
      }

      $form = $formFactory->createForm();

      $form->setData($user);
      $form->handleRequest($request);
      if ($form->isValid()) {
          $em = $this->getDoctrine()->getManager();
          $user->setEnabled(true);
          $em->persist($user);
          $em->flush();

          return $this->redirect($this->generateUrl('fos_user_security_login'));


      }

      return $this->render('FOSUserBundle:Registration:register.html.twig', array(
          'form' => $form->createView(),
      ));
  }

  /**
   * Tell the user his account is now confirmed.
   */
  public function confirmedAction()
  {
      $user = $this->getUser();
      return $this->render('FOSUserBundle:Registration:confirmed.html.twig', array(
          "user" => $user
      ));
  }



    public function loginAction(Request $request) {

    
          /** @var $session \Symfony\Component\HttpFoundation\Session\Session */
          $session = $request->getSession();

          if (class_exists('\Symfony\Component\Security\Core\Security')) {
              $authErrorKey = Security::AUTHENTICATION_ERROR;
              $lastUsernameKey = Security::LAST_USERNAME;
          } else {
              // BC for SF < 2.6
              $authErrorKey = SecurityContextInterface::AUTHENTICATION_ERROR;
              $lastUsernameKey = SecurityContextInterface::LAST_USERNAME;
          }

          // get the error if any (works with forward and redirect -- see below)
          if ($request->attributes->has($authErrorKey)) {
              $error = $request->attributes->get($authErrorKey);
          } elseif (null !== $session && $session->has($authErrorKey)) {
              $error = $session->get($authErrorKey);
              $session->remove($authErrorKey);
          } else {
              $error = null;
          }

          if (!$error instanceof AuthenticationException) {
              $error = null; // The value does not come from the security component.
          }


          // last username entered by the user
          $lastUsername = (null === $session) ? '' : $session->get($lastUsernameKey);

          if ($this->has('security.csrf.token_manager')) {
              $csrfToken = $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue();
          } else {
              // BC for SF < 2.4
              $csrfToken = $this->has('form.csrf_provider')
                  ? $this->get('form.csrf_provider')->generateCsrfToken('authenticate')
                  : null;
          }

          return $this->renderLogin(array(
              'breadcrumb' => [
                  'title' => 'Espace utilisateur',
                  'subtitle' => 'Se connecter'
              ],
              'last_username' => $lastUsername,
              'error' => $error,
              'csrf_token' => $csrfToken,
          ));
      }

      /**
       * Renders the login template with the given parameters. Overwrite this function in
       * an extended controller to provide additional data for the login template.
       *
       * @param array $data
       *
       * @return \Symfony\Component\HttpFoundation\Response
       */
      protected function renderLogin(array $data)
      {
          return $this->render('FOSUserBundle:Security:login.html.twig', $data);
      }

      public function checkAction()
      {
          throw new \RuntimeException('You must configure the check path to be handled by the firewall using form_login in your security firewall configuration.');
      }

      public function logoutAction()
      {
          throw new \RuntimeException('You must activate the logout in your security firewall configuration.');
      }
      }
